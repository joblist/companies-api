module.exports = (req, res) => {
    res.status(200).json({
	urlEndpoint: '/url',
	companiesEndpoint: '/companies'
    })
}
