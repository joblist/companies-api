const fs = require('fs')
const fetch = require('isomorphic-fetch')
const cheerio = require('cheerio')

const workersArtifactsCompaniesUrl = 'https://gitlab.com/joblist/workers/-/jobs/artifacts/master/raw/public/companies.json?job=api_companies:on-schedule'

const getCompanies = async (req, res) => {
    let companies
    try {
	companies = await fetch(workersArtifactsCompaniesUrl)
	    .then(res => {
		console.log(res)
		return res.json()
	    })
    } catch (error) {
	console.log('Error fetching companies', error)
    }

    if (!companies) {
	return res.status(400).json({
	    error: 'Could not find companies'
	})
    }

    return res.status(200).json(companies)
}

module.exports = getCompanies
