const fetch = require('isomorphic-fetch')
const cheerio = require('cheerio')


const handleUrl = async () => {
    let goalsLinks
    try {
	goalsLinks = await getGoalsLinksFromHome()
    } catch (error) {
	console.log('Error getting goals links', error)
	return
    }

    let goals
    try {
	goals = await getGoals(goalsLinks.slice(0, goalsLinks.length - 1))
    } catch (error) {
	console.log('Error building goals data', error)
	return
    }

    return {
	/* goalsLinks, */
	goals
    }
}

const getGoalsLinksFromHome = async () => {
    const unUrl = 'https://www.un.org/sustainabledevelopment/'

    let page
    try {
	page = await fetchUrl(unUrl)
    } catch (err) {
	throw Error(err)
    }
    
    const $ = cheerio.load(page)

    const $menuSelector = '#sdg-cards'
    const $links = $('a', $menuSelector)

    let links = []
    $links.each((index, item) => {
	links[index] = item.attribs.href
    })

    return links
}

const getGoals = async (goalsLinks) => {
    const goalsPromises = goalsLinks.map(link => {
	return fetchUrl(link)
    })

    let goalsPages
    try {
	goalsPages = await Promise.all(goalsPromises)
    } catch (error) {
	console.log('Error getting goals pages', error)
	return []
    }

    let goalsData = goalsPages.map((page, index) => {
	const $ = cheerio.load(page)
	const containerSelector = '.post-content'

	/* find targets in dom */
	const targetsContainerSelector = `[aria-labelledby=fusion-tab-goal${index + 1}targets] p`
	const $targets = $(targetsContainerSelector, containerSelector)
	let targets = []
	$targets.each((i, item) => {
	    const target = {}
	    target['id'] = i + 1
	    if (item.children.length > 0 && item.children[1]) {
		target['data'] = item.children[1].data
	    }
	    targets[i] = target
	})

	/* find content in dom */
	const contentSelector = '.fusion-text.fusion-text-1'
	const $contents = $(contentSelector, containerSelector)

	const url = goalsLinks[index]

	const content = $contents.text()
	return {
	    url,
	    content,
	    targets,
	}
    })
    
    return goalsData
}


const fetchUrl = async (url) => {
    let data
    try {
	data = await fetch(url).then(res => {
	    return res.text()
	})
    } catch (err) {
	throw Error(`Could not fetch url ${url}; is it alive?`)
    }
    return data
}



const getInfoFromUrl = async (req, res) => {
    let data
    try {
	data = await handleUrl()
    } catch (err) {
	return res.status(400).json({
	    error: err.message,
	    code: err.code
	})
    }   

    return res.status(200).json(data)
}

module.exports = getInfoFromUrl
