const fs = require('fs')
const fetch = require('isomorphic-fetch')
const cheerio = require('cheerio')

const urlExpression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi

const getHostname = hostname => {
    const s = hostname.split('.')
    // not the domain name (.de), but the one before
    return s[s.length - 2]
}

const handleCompanyUrl = async url => {
    try {
	url = new URL(url)
    } catch (err) {
	throw Error(err)
    }
    const hostname = getHostname(url.hostname)

    let page
    try {
	page = await fetchCompany(url.href)
    } catch (err) {
	throw Error(err)
    }

    const $ = cheerio.load(page)

    let links
    try {
	links = await findCompanyLinks($, url.href)
    } catch (err) {
	throw Error(err)
    }

    let meta
    try {
	meta = await findCompanyMeta($, links)
    } catch (err) {
	throw Error(err)
    }
    
    const company = {
	url: url.href,
	hostname: hostname,
	title: meta.title,
	description: meta.description,
	links
    }
    return company
}

const findCompanyMeta = ($, links) => {
    const head = $('head')
    const title = $('title', head).text()
    const description = $('meta[name=description]', head).attr('content')

    return {
	title,
	description
    }
    
}

const findCompanyLinks = async ($, url) => {
    const $links = $('a')

    let matchedLinks = {
	jobs: [],
	instagram: [],
	facebook: [],
	linkedin: [],
	twitter: []
    }

    $links.each((index, item) => {
	const baseUrl = item.attribs.href
	if (typeof baseUrl !== 'string') return

	const matches = [
	    findCareerUrl(baseUrl),
	    linkedinUrl(baseUrl),
	    twitterUrl(baseUrl),
	    findFacebookUrl(baseUrl),
	    findInstagramUrl(baseUrl)
	].filter(match => match)
	 .map(match => serializeMatch(match, url))

	if (!matches.length) return

	matches.forEach(match => {
	    if (!matchedLinks[match.id].includes(match.url)) {
		matchedLinks[match.id].push(match.url)
	    }
	})
    })

    return matchedLinks
}

const serializeMatch = (match, hostUrl) => {
    let url
    try {
	url = new URL(match.url)
    } catch (err) {
	if (match.url.startsWith('/')) {
	    url = new URL(hostUrl + match.url.substring(1))
	    match.url = url.href
	} else {
	    throw Error(`Cannot serialize url, ${match.url}`)
	}
    }

    if (url.href.startsWith('javascript:')) {
	const regex = new RegExp(urlExpression)

	const jsHref = url.href.match(regex)
	if (jsHref) {
	    match.url = jsHref[0]
	}
    }

    return match
}

const fetchCompany = async (url) =>{
    let data
    try {
	data = await fetch(url).then(res => {
	    return res.text()
	})
    } catch (err) {
	throw Error(`Could not fetch url ${url}; is it alive?`)
    }
    return data
}

const findCareerUrl = url => {
    if (
	url.indexOf('work') < 0
	&& url.indexOf('jobs') < 0
	&& url.indexOf('career') < 0
    ) return null

    return {
	id: 'jobs',
	url: url
    }
}

const findInstagramUrl = url => {
    if (url.indexOf('instagram') < 0) return null
    return {
	id: 'instagram',
	url: url
    }
}

const findFacebookUrl = url => {
    if (url.indexOf('facebook') < 0) return null
    return {
	id: 'facebook',
	url: url
    }
}

const linkedinUrl = url => {
    if (url.indexOf('linkedin') < 0) return null
    return {
	id: 'linkedin',
	url: url
    }
}

const twitterUrl = url => {
    if (url.indexOf('twitter') < 0) return null
    return {
	id: 'twitter',
	url: url
    }
}

const getInfoFromUrl = async (req, res) => {
    const {
	query: { url }
    } = req

    if (!url) {
	return res.status(200).json({
	    usage: '?url=https://example.com'
	})
    }

    let company
    try {
	company = await handleCompanyUrl(url)
    } catch (err) {
	return res.status(400).json({
	    error: err.message,
	    code: err.code
	})
    }

    return res.status(200).json(company)
}

module.exports = getInfoFromUrl
