# companies-api

This is a version at a tiny API for [api.joblist.city](https://api.joblist.city).

## Endpoints.

- `/` welcome message
- `/api` api routes presentation
- `/companies` return all companies
- `/url?url=` try to enrich a company data from its URL

> Note: endpoints are all experimental and will be subject to change; very unstable.

### `/companies`

Returns an array of companies and their data.

This endpoint uses in the end the data from
[github/joblistcity/companies](https://github.com/joblistcity/companies),
which has been stored as an artifact JSON file by a gitlab workers in
the repo
[`gitlab/joblist/workers`](https://gitlab.com/joblist/workers/-/blob/master/.gitlab-ci.yml)
*job@*`api_companies:on-schedule`.

To check out the possible keys of a company, check out the model
definition in the [netlify-cms
instance](https://gitlab.com/joblist/edit-companies/-/blob/master/js/models/companies.js).

> Note: Companies might not have all model keys in the response, only
> the ones with data. This is a result of netlify-cms storing the data
> in mardown files, and the worker reading from these files with
> npm:@github-docs/frontmatter. Sorry for the inconvenience.

### `/url`

> This API enpoint particularely is very unstable, as not jused for
> anything right now. In the end, it should be able to help fill a new
> company form (as in netlify-cms or PR), for all fields.

Try to enrich `company` data from a URL.

Params:
- `?url=[url string]` a URL pointing to a company main website.

Returns: JSON Object (of the sort)
```
{
  "url": "https://port-zero.com/",
  "hostname": "port-zero",
  "title": "Port Zero",
  "description": "IT-Security and software development",
  "links": {
    "jobs": [
      "https://port-zero.com/jobs/"
    ],
    "instagram": [],
    "facebook": [],
    "linkedin": [
      "https://www.linkedin.com/company/5061707/"
    ],
    "twitter": [
      "https://twitter.com/port_zer0"
    ]
  }
}
``` 

** Development.

- dev: 
- prodction: =now . --prod=

* local build
- =now dev=
